<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:18
 */

namespace Fixture;




use App\Fixture\WpPostFixtureBase;

class WpPostFixture extends WpPostFixtureBase
{
    function createData()
    {
        $this->createPost(
            $this->getReference('wpUser-admin')->getId(),
            'hello-world',
            'Hello world!',
            '<!-- wp:paragraph -->
<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>
<!-- /wp:paragraph -->',
            'post'
            );
    }
}