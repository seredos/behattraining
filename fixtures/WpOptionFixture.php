<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:24
 */

namespace Fixture;


use App\Fixture\WpOptionFixtureBase;

class WpOptionFixture extends WpOptionFixtureBase
{
    function createData()
    {
        parent::createData();
        $this->createOptionByArray([
            'siteurl' => 'http://127.0.0.1',
            'home' => 'http://127.0.0.1',
            'blogname' => 'test',
            'blogdescription' => 'Just another WordPress site',
            'users_can_register' => 0,
            'admin_email' => 'admin@web.de',
            'start_of_week' => 1,
            'use_balanceTags' => 0,
            'use_smilies' => 1,
            'require_name_email' => 1,
            'comments_notify' => 1,
            'posts_per_rss' => 10,
            'rss_use_excerpt' => 0,
            'mailserver_url' => 'mail.example.com',
            'mailserver_login' => 'login@example.com',
            'mailserver_pass' => 'password',
            'mailserver_port' => 110,
            'dedault_category' => 1,
            'default_comment_status' => 'open',
            'default_ping_status' => 'open',
            'default_pingback_flag' => 1,
            'posts_per_page' => 10,
            'date_format' => 'F j, Y',
            'time_format' => 'g:i a',
            'links_updated_date_format' => 'F j, Y g:i a',
            'default_category' => 1,
            'comment_moderation' => 0,
            'moderation_notify' => 1,
            'permalink_structure' => '',
            'rewrite_rules' => '',
            'hack_file' => 0,
            'blog_charset' => 'UTF-8',
            'active_plugins' => ['relative-url.php'],
            'category_base' => '',
            'ping_sites' => 'http://rpc.pingomatic.com/',
            'comment_max_links' => 2,
            'gmt_offset' => 0,
            'default_email_category' => 1,
            'template' => 'twentynineteen',
            'stylesheet' => 'twentynineteen',
            'comment_whitelist' => 1,
            'comment_registration' => 0,
            'html_type' => 'text/html',
            'use_trackback' => 0,
            'default_role' => 'subscriber',
            'db_version' => '43764',
            'uploads_use_yearmonth_folders' => 1,
            'upload_path' => '',
            'blog_public' => 1,
            'default_link_category' => 0,
            'show_on_front' => 'posts',
            'tag_base' => '',
            'show_avatars' => 1,
            'avatar_rating' => 'G',
            'upload_url_path' => '',
            'thumbnail_size_w' => 150,
            'thumbnail_size_h' => 150,
            'thumbnail_crop' => 1,
            'medium_size_w' => 300,
            'medium_size_h' => 300,
            'avatar_default' => 'mystery',
            'large_size_w' => 1024,
            'large_size_h' => 1024,
            'image_default_link_type' => 'none',
            'image_default_size' => '',
            'image_default_align' => '',
            'close_comments_for_old_posts' => 0,
            'close_comments_days_old' => 14,
            'thread_comments' => 1,
            'thread_comments_depth' => 5,
            'page_comments' => 0,
            'comments_per_page' => 50,
            'default_comments_page' => 'newest',
            'comment_order' => 'asc',
            'sticky_posts' => [],
            'timezone_string' => '',
            'page_for_posts' => 0,
            'page_on_front' => 0,
            'default_post_format' => 0,
            'link_manager_enabled' => 0,
            'finished_splitting_shared_terms' => 0,
            'site_icon' => 0,
            'medium_large_size_w' => 768,
            'medium_large_size_h' => 0,
            'wp_page_for_privacy_policy' => 0,
            'show_comments_cookies_opt_in' => 0,
            'initial_db_version' => 43764,
            'WPLANG' => [],
            'db_upgraded' => 1
        ]);

        $this->createOption('wp_user_roles',['administrator' =>
            array (
                'name' => 'Administrator',
                'capabilities' =>
                    array (
                        'switch_themes' => true,
                        'edit_themes' => true,
                        'activate_plugins' => true,
                        'edit_plugins' => true,
                        'edit_users' => true,
                        'edit_files' => true,
                        'manage_options' => true,
                        'moderate_comments' => true,
                        'manage_categories' => true,
                        'manage_links' => true,
                        'upload_files' => true,
                        'import' => true,
                        'unfiltered_html' => true,
                        'edit_posts' => true,
                        'edit_others_posts' => true,
                        'edit_published_posts' => true,
                        'publish_posts' => true,
                        'edit_pages' => true,
                        'read' => true,
                        'level_10' => true,
                        'level_9' => true,
                        'level_8' => true,
                        'level_7' => true,
                        'level_6' => true,
                        'level_5' => true,
                        'level_4' => true,
                        'level_3' => true,
                        'level_2' => true,
                        'level_1' => true,
                        'level_0' => true,
                        'edit_others_pages' => true,
                        'edit_published_pages' => true,
                        'publish_pages' => true,
                        'delete_pages' => true,
                        'delete_others_pages' => true,
                        'delete_published_pages' => true,
                        'delete_posts' => true,
                        'delete_others_posts' => true,
                        'delete_published_posts' => true,
                        'delete_private_posts' => true,
                        'edit_private_posts' => true,
                        'read_private_posts' => true,
                        'delete_private_pages' => true,
                        'edit_private_pages' => true,
                        'read_private_pages' => true,
                        'delete_users' => true,
                        'create_users' => true,
                        'unfiltered_upload' => true,
                        'edit_dashboard' => true,
                        'update_plugins' => true,
                        'delete_plugins' => true,
                        'install_plugins' => true,
                        'update_themes' => true,
                        'install_themes' => true,
                        'update_core' => true,
                        'list_users' => true,
                        'remove_users' => true,
                        'promote_users' => true,
                        'edit_theme_options' => true,
                        'delete_themes' => true,
                        'export' => true,
                    ),
            ),
            'editor' =>
                array (
                    'name' => 'Editor',
                    'capabilities' =>
                        array (
                            'moderate_comments' => true,
                            'manage_categories' => true,
                            'manage_links' => true,
                            'upload_files' => true,
                            'unfiltered_html' => true,
                            'edit_posts' => true,
                            'edit_others_posts' => true,
                            'edit_published_posts' => true,
                            'publish_posts' => true,
                            'edit_pages' => true,
                            'read' => true,
                            'level_7' => true,
                            'level_6' => true,
                            'level_5' => true,
                            'level_4' => true,
                            'level_3' => true,
                            'level_2' => true,
                            'level_1' => true,
                            'level_0' => true,
                            'edit_others_pages' => true,
                            'edit_published_pages' => true,
                            'publish_pages' => true,
                            'delete_pages' => true,
                            'delete_others_pages' => true,
                            'delete_published_pages' => true,
                            'delete_posts' => true,
                            'delete_others_posts' => true,
                            'delete_published_posts' => true,
                            'delete_private_posts' => true,
                            'edit_private_posts' => true,
                            'read_private_posts' => true,
                            'delete_private_pages' => true,
                            'edit_private_pages' => true,
                            'read_private_pages' => true,
                        ),
                ),
            'author' =>
                array (
                    'name' => 'Author',
                    'capabilities' =>
                        array (
                            'upload_files' => true,
                            'edit_posts' => true,
                            'edit_published_posts' => true,
                            'publish_posts' => true,
                            'read' => true,
                            'level_2' => true,
                            'level_1' => true,
                            'level_0' => true,
                            'delete_posts' => true,
                            'delete_published_posts' => true,
                        ),
                ),
            'contributor' =>
                array (
                    'name' => 'Contributor',
                    'capabilities' =>
                        array (
                            'edit_posts' => true,
                            'read' => true,
                            'level_1' => true,
                            'level_0' => true,
                            'delete_posts' => true,
                        ),
                ),
            'subscriber' =>
                array (
                    'name' => 'Subscriber',
                    'capabilities' =>
                        array (
                            'read' => true,
                            'level_0' => true,
                        ),
                ),
            'test' =>
                array (
                    'name' => 'Test',
                    'capabilities' =>
                        array (
                            'read' => true,
                        ),
                ),
            'poster' =>
                array (
                    'name' => 'Poster',
                    'capabilities' =>
                        array (
                            'read' => true,
                            'edit_posts' => true
                        ),
                ),
            'publisher' =>
                array (
                    'name' => 'Publisher',
                    'capabilities' =>
                        array (
                            'read' => true,
                            'publish_posts' => true,
                            'edit_posts' => true,
                            'edit_others_posts' => true,
                            'delete_posts' => true,
                            'delete_published_posts' => true,
                            'delete_others_posts' => true
                        ),
                )]);
    }
}