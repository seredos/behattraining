<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:08
 */

namespace Fixture;


use App\Fixture\WpUserMetaFixtureBase;

class WpUserMetaFixture extends WpUserMetaFixtureBase
{
    /**
     * @throws \Exception
     */
    function createData()
    {
        $this->createUserMetaArray(
            $this->getReference('wpUser-admin')->getId()
            , [
                'nickname' => 'admin',
                'first_name' => '',
                'last_name' => '',
                'description' => '',
                'rich_editing' => 'false',
                'syntax_highlighting' => 'true',
                'comment_shortcuts' => 'false',
                'admin_color' => 'fresh',
                'use_ssl' => 0,
                'show_admin_bar_front' => 'true',
                'locale' => '',
                'wp_capabilities' => [
                    'administrator' => 1
                ],
                'wp_user_level' => 10,
                'dismissed_wp_pointers' => 'wp496_privacy',
                'show_welcome_panel' => 1
        ]);

        $this->createUserMetaArray(
            $this->getReference('wpUser-test')->getId()
            , [
            'nickname' => 'test',
            'first_name' => '',
            'last_name' => '',
            'description' => '',
            'rich_editing' => 'true',
            'syntax_highlighting' => 'true',
            'comment_shortcuts' => 'false',
            'admin_color' => 'fresh',
            'use_ssl' => 0,
            'show_admin_bar_front' => 'true',
            'locale' => '',
            'wp_capabilities' => [
                'test' => 1
            ],
            'wp_user_level' => 10,
            'dismissed_wp_pointers' => 'wp496_privacy',
            'show_welcome_panel' => 1
        ]);

        $this->createUserMetaArray(
            $this->getReference('wpUser-poster')->getId()
            , [
            'nickname' => 'poster',
            'first_name' => '',
            'last_name' => '',
            'description' => '',
            'rich_editing' => 'false',
            'syntax_highlighting' => 'true',
            'comment_shortcuts' => 'false',
            'admin_color' => 'fresh',
            'use_ssl' => 0,
            'show_admin_bar_front' => 'true',
            'locale' => '',
            'wp_capabilities' => [
                'poster' => 1
            ],
            'wp_user_level' => 10,
            'dismissed_wp_pointers' => 'wp496_privacy',
            'show_welcome_panel' => 1
        ]);

        $this->createUserMetaArray(
            $this->getReference('wpUser-publisher')->getId()
            , [
            'nickname' => 'publisher',
            'first_name' => '',
            'last_name' => '',
            'description' => '',
            'rich_editing' => 'false',
            'syntax_highlighting' => 'true',
            'comment_shortcuts' => 'false',
            'admin_color' => 'fresh',
            'use_ssl' => 0,
            'show_admin_bar_front' => 'true',
            'locale' => '',
            'wp_capabilities' => [
                'publisher' => 1
            ],
            'wp_user_level' => 10,
            'dismissed_wp_pointers' => 'wp496_privacy',
            'show_welcome_panel' => 1
        ]);
    }
}