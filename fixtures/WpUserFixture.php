<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 16:48
 */

namespace Fixture;



use App\Fixture\WpUserFixtureBase;

class WpUserFixture extends WpUserFixtureBase
{
    function createData()
    {
        $this->createUser(
            'admin',
            'admin',
            'admin',
            'admin@web.de',
            'admin'
        );

        $this->createUser(
            'test',
            'test',
            'test',
            'test@web.de',
            'test'
        );

        $this->createUser(
            'poster',
            'poster',
            'poster',
            'poster@web.de',
            'poster'
        );

        $this->createUser(
            'publisher',
            'publisher',
            'publisher',
            'publisher@web.de',
            'publisher'
        );
    }
}