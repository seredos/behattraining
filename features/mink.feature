# language: de
Funktionalität: Mink (Selenium & Goutte)

  @testrail-case-6 @javascript @screen_1680x1050
  Szenario: Mit welchem Browser arbeite ich?
    Wenn ich auf der Seite "https://detectmybrowser.com/" bin
    Dann sollte ich einen Screenshot machen