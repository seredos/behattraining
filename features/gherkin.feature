# language: de
Funktionalität: Gherkin Language

  Grundlage:
    Angenommen ich bin auf der Welt

  @testrail-case-1 @priority_low
  Szenario: ein Szenario ohne Parameter
    Wenn ich hallo Computer sage
    Dann sehe ich hallo User

  @testrail-case-2 @priority_low
  Szenario: ein weiteres Szenario ohne Parameter
    Wenn ich hallo Universum sage
    Dann sehe ich hallo Wurm

  @testrail-case-3 @priority_low
  Szenario: ein Szenario mit Parametern
    Wenn ich "hallo Computer" sage
    Dann sehe ich "hallo User"

  @testrail-case-4 @priority_low
  Szenario: ein weiteres Szenario mit Parametern
    Wenn ich "hallo Universum" sage
    Dann sehe ich "hallo Wurm"

  @testrail-case-5 @priority_low
  Szenario: Parameter und Tabellen
    Angenommen ich sehe die Planeten "Merkur", "Venus", "Erde", "Mars", "Jupiter", "Saturn", "Uranus" und "Neptun"
    Angenommen ich sehe die folgenden Planeten
      | Name    | Gravity    |
      | Merkur  | 3,70 m/s²  |
      | Venus   | 8,87 m/s²  |
      | Erde    | 9,80 m/s²  |
      | Mars    | 3,69 m/s²  |
      | Jupiter | 24,79 m/s² |
      | Saturn  | 10,44 m/s² |
      | Uranus  | 8,87 m/s²  |
      | Neptun  | 11,15 m/s² |

  @priority_low @testrail-case-7
  Szenario: Mehrzeiliger Parameter
    Angenommen ich sehe den folgenden Text
      """
      Dies ist ein Text
      mit mehreren
      Zeilen
      """