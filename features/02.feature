# language: de
Funktionalität: Hier wird offenbart, wie wir uns mittels Mink auf einer Seite bewegen können. Außerdem werden die
  ersten Angenommen Bedingungen definiert

  @javascript @testrail-case-9
  Szenario: ein erster Login
    Angenommen der Benutzer "test" existiert
    Und der Benutzer "test" hat die Rolle "test"
    Und die Rolle "test" hat die Berechtigung "read"
    Wenn ich auf der Wordpress Seite "wp-login.php" bin
    Und ich in das Feld "Username or Email Address" den Wert "test" eingebe
    Und ich in das Feld "Password" den Wert "test" eingebe
    Und ich auf den Button "Log In" klicke
    Dann sollte ich den Text "Dashboard" sehen

  @testrail-case-10
  Szenario: ein Login shortcut Step
    Angenommen ich habe mich mit "admin" "admin" angemeldet