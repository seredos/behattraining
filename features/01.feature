# language: de
Funktionalität: Dieser erste Test zeigt die Erstellung eines ersten Tests und offenbart auch gleich einen
  schlechtes Design-Pattern. Steps dürfen nicht unmittelbar von Parametern aus vorherigen Steps abhängen!

  @testrail-case-8
  Szenario: hallo schlechter gherkin test
    Wenn ich den folgenden Reim sage
      """
      Was bringt den Arzt um sein Brot?
      A das Leben, B der Tod
      Drum hällt er dich, auf das er lebe,
      zwischen beidem in der schwebe
      """
    Dann habe ich den folgenden Reim gesagt
      """
      Was bringt den Arzt um sein Brot?
      A das Leben, B der Tod
      Drum hällt er dich, auf das er lebe,
      zwischen beidem in der schwebe
      """