# language: de
Funktionalität: Ein erster Entwurf für das erstellen und prüfen eines Posts
  noch nicht schön aber funktioniert schon mal

  @javascript @testrail-case-11
  Szenario: irgendwie zum ziel
    Angenommen ich habe mich mit "admin" "admin" angemeldet
    Wenn ich auf der Wordpress Seite "wp-admin/post-new.php" bin
    Und ich in das Feld "Add title" den Wert "ein simpler title" eingebe
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Publish" klicke
    Dann sollte ich den Text "Post published." sehen
    Wenn ich auf den Link "ein simpler title" klicke
    Dann sollte ich den Text "ein simpler title" sehen