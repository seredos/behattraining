# language: de
Funktionalität: Zu guter letzt, werden die verschiedenen Teilbereiche zu einem Vollständigen Prozess zusammen gesetzt.

  @javascript @testrail-case-20
  Szenario: ich als Poster erstelle einen Post und, Sende ihn zum Review
    Angenommen der Benutzer "poster" existiert
    Und der Benutzer "poster" hat die Rolle "poster"
    Und die Rolle "poster" hat die Berechtigung "read"
    Und die Rolle "poster" hat die Berechtigung "edit_posts"
    Und für den Benutzer "poster" ist der Visual Editor deaktiviert
    Und ich habe mich mit "poster" "poster" angemeldet
    Wenn ich auf der Wordpress Seite "wp-admin/post-new.php" bin
    Und ich in das Feld "Add title" den Wert "ein kompletter post prozess" eingebe
    Und ich in das Feld "Start writing with text or HTML" den Wert "der inhalt eines kompletten post prozesses" eingebe
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Submit for Review" klicke
    Dann sollte ich den Text "Saved" sehen

  @javascript @testrail-case-21
  Szenario: ich als Unbekannter Benutzer kann den Post nicht sehen, da er noch nicht veröffentlicht wurde
    Wenn ich auf der Wordpress Seite "index.php" bin
    Dann sollte ich den Text "ein kompletter post prozess" nicht sehen

  @javascript @testrail-case-22
  Szenario: ich als Publisher lese den Post, übernehme die Verantwortlichkeit und veröffentliche ihn
    Angenommen der Benutzer "publisher" existiert
    Und der Benutzer "publisher" hat die Rolle "publisher"
    Und die Rolle "publisher" hat die Berechtigung "read"
    Und die Rolle "publisher" hat die Berechtigung "edit_posts"
    Und die Rolle "publisher" hat die Berechtigung "edit_others_posts"
    Und die Rolle "publisher" hat die Berechtigung "publish_posts"
    Und ich habe mich mit "publisher" "publisher" angemeldet
    Wenn ich den Post "ein kompletter post prozess" zur Bearbeitung öffne
    Und ich in dem Feld "Add title" den Wert "ein kompletter post prozess" sehe
    Und ich auf den Link "Take Over" klicke
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Publish" klicke
    Dann sollte ich den Text "Post published" sehen

  @javascript @testrail-case-23
  Szenario: ich als Unbekannter Benutzer kann den veröffentlichten Post lesen
    Angenommen es gibt mindestens zwei publizierte Posts
    Wenn ich auf der Wordpress Seite "index.php" bin
    Dann sehe ich "ein kompletter post prozess" als ersten Post
    Und ich sehe mehrere Posts
    Und ich sehe, dass der Post "ein kompletter post prozess" den Inhalt "der inhalt eines kompletten post prozesses" hat
    Und ich sehe, dass der Post "ein kompletter post prozess" von dem Benutzer "poster" erstellt wurde
    Und ich sehe, dass der Post "ein kompletter post prozess" heute erstellt wurde

  @javascript @testrail-case-24
  Szenario: ich als publisher kann diesen Post nun wieder entfernen
    Angenommen der Benutzer "publisher" existiert
    Und der Benutzer "publisher" hat die Rolle "publisher"
    Und die Rolle "publisher" hat die Berechtigung "delete_posts"
    Und die Rolle "publisher" hat die Berechtigung "delete_others_posts"
    Und die Rolle "publisher" hat die Berechtigung "delete_published_posts"
    Und ich habe mich mit "publisher" "publisher" angemeldet
    Wenn ich auf der Wordpress Seite "wp-admin/edit.php" bin
    Und ich mich mit der Maus über dem Element "ein kompletter post prozess" befinde
    Und ich auf den Link "Trash" klicke
    Dann sollte ich den Text "1 post moved to the Trash." sehen