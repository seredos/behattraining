# language: de
Funktionalität: Eine andere Variante. Nicht wirklich besser, aber anders.
  In diesem Fall gehen wir nicht direkt auf die Seite zum erstellen eines Posts,
  sondern wir navigieren uns vom Dashboard aus zu der Seite.

  @javascript @testrail-case-12
  Szenario: vom Dashboard zum fertigen Post
    Angenommen ich habe mich mit "admin" "admin" angemeldet
    Und für den Benutzer "admin" ist der Visual Editor deaktiviert
    Wenn ich das Menü "New" öffne
    Und ich auf den Link "Post" klicke
    Und ich in das Feld "Add title" den Wert "ein simpler zweiter title" eingebe
    Und ich in das Feld "Start writing with text or HTML" den Wert "ein simpler inhalt" eingebe
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Publish" klicke
    Dann sollte ich den Text "Post published." sehen
    Wenn ich auf den Link "ein simpler zweiter title" klicke
    Dann sollte ich den Text "ein simpler zweiter title" sehen
    Und ich sollte den Text "ein simpler inhalt" sehen