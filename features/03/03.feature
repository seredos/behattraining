# language: de
Funktionalität: Eine weitere Variation des ersten Tests. Beim schreiben der ersten Variante ist aufgefallen,
  das sich der Post-Editor von Wordpress in verschiedenen Modis befinden kann.
  unser Standard-Feld-Eingabe Step funktioniert mit dem "Visual Editor" aber nicht.
  Für das Testen dieses Editors müsste ein Seperater Step implementiert werden.
  Desweiteren Versperren die Tooltips die Sicht auf die Seite.
  Aus diesem Grund wird ein "Cheat"-Step implementiert, welcher die Tip-Funktion über die Browser-Cookies
  deaktiviert

  @javascript @testrail-case-13
  Szenario: prüfen des Visual-Editor Status und deaktivieren der Tips für die Session
    Angenommen ich habe mich mit "admin" "admin" angemeldet
    Und für den Benutzer "admin" ist der Visual Editor deaktiviert
    Und der Benutzer "admin" hat die Tips für seine Session deaktiviert
    Wenn ich auf den Link "New" klicke
    Und ich in das Feld "Add title" den Wert "ein simpler dritter title" eingebe
    Und ich in das Feld "Start writing with text or HTML" den Wert "ein simpler dritter inhalt" eingebe
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Publish" klicke
    Dann sollte ich den Text "Post published." sehen
    Wenn ich auf den Link "View Post" klicke
    Dann sollte ich den Text "ein simpler dritter title" sehen
    Und ich sollte den Text "ein simpler dritter inhalt" sehen