# language: de
Funktionalität: Nun konzentrieren wir uns mehr auf die Überprüfung der Anzeige im Frontend
  Welche Angenommen Bedingungen gibt es und wie können wir die Posts besser überprüfen

  @javascript @testrail-case-18
  Szenario: zuerst brauchen wir einen neuen post
    Angenommen ich habe mich mit "admin" "admin" angemeldet
    Und für den Benutzer "admin" ist der Visual Editor deaktiviert
    Wenn ich auf der Wordpress Seite "wp-admin/post-new.php" bin
    Und ich in das Feld "Add title" den Wert "ein simpler vierter title" eingebe
    Und ich in das Feld "Start writing with text or HTML" den Wert "ein simpler inhalt" eingebe
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Publish" klicke
    Dann sollte ich den Text "Post published." sehen

  @testrail-case-19
  Szenario: nun prüfen wir das Frontend etwas genauer
    Angenommen der Post "ein simpler vierter title" ist der neueste Post
    Und der Post "ein simpler vierter title" wurde heute erstellt
    Und der Post "ein simpler vierter title" wurde publiziert
    Und der Post "ein simpler vierter title" wurde von dem Benutzer "admin" erstellt
    Und der Post "ein simpler vierter title" hat den Inhalt "ein simpler inhalt"
    Und es gibt mindestens zwei publizierte Posts
    Wenn ich auf der Wordpress Seite "index.php" bin
    Dann sehe ich "ein simpler vierter title" als ersten Post
    Und ich sehe mehrere Posts
    Und ich sehe, dass der Post "ein simpler vierter title" den Inhalt "ein simpler inhalt" hat
    Und ich sehe, dass der Post "ein simpler vierter title" von dem Benutzer "admin" erstellt wurde
    Und ich sehe, dass der Post "ein simpler vierter title" heute erstellt wurde