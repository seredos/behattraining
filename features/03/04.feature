# language: de
Funktionalität: Jetzt knüpfen wir uns das Berechtigungssystem vor was wird gebraucht, damit es funktioniert.
  Durch das durchforsten des Berechtigungssystems offenbart sich ein publish prozess, der versucht wird abzubilden

  @javascript @testrail-case-14
  Szenario: ich als Poster erstelle einen Post und, Sende ihn zum Review
    Angenommen der Benutzer "poster" existiert
    Und der Benutzer "poster" hat die Rolle "poster"
    Und die Rolle "poster" hat die Berechtigung "read"
    Und die Rolle "poster" hat die Berechtigung "edit_posts"
    Und für den Benutzer "poster" ist der Visual Editor deaktiviert
    Und ich habe mich mit "poster" "poster" angemeldet
    Wenn ich auf der Wordpress Seite "wp-admin/post-new.php" bin
    Und ich in das Feld "Add title" den Wert "ein simpler poster title" eingebe
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Submit for Review" klicke
    Dann sollte ich den Text "Saved" sehen

  @javascript @testrail-case-15
  Szenario: ich als Publisher lese den Post, übernehme die Verantwortlichkeit und veröffentliche ihn
    Angenommen der Benutzer "publisher" existiert
    Und der Benutzer "publisher" hat die Rolle "publisher"
    Und die Rolle "publisher" hat die Berechtigung "read"
    Und die Rolle "publisher" hat die Berechtigung "edit_posts"
    Und die Rolle "publisher" hat die Berechtigung "edit_others_posts"
    Und die Rolle "publisher" hat die Berechtigung "publish_posts"
    Und ich habe mich mit "publisher" "publisher" angemeldet
    Wenn ich den Post "ein simpler poster title" zur Bearbeitung öffne
    Und ich in dem Feld "Add title" den Wert "ein simpler poster title" sehe
    Und ich auf den Link "Take Over" klicke
    Und ich auf den Button "Publish…" klicke
    Und ich auf den Button "Publish" klicke
    Dann sollte ich den Text "Post published" sehen

  @javascript @testrail-case-16
  Szenario: ich als Unbekannter Benutzer kann den veröffentlichten Post lesen
    Wenn ich auf der Wordpress Seite "index.php" bin
    Dann sollte ich den Text "ein simpler poster title" sehen

  @javascript @testrail-case-17
  Szenario: ich als publisher kann diesen Post nun wieder entfernen
    Angenommen der Benutzer "publisher" existiert
    Und der Benutzer "publisher" hat die Rolle "publisher"
    Und die Rolle "publisher" hat die Berechtigung "read"
    Und die Rolle "publisher" hat die Berechtigung "edit_posts"
    Und die Rolle "publisher" hat die Berechtigung "delete_posts"
    Und die Rolle "publisher" hat die Berechtigung "delete_others_posts"
    Und die Rolle "publisher" hat die Berechtigung "delete_published_posts"
    Und ich habe mich mit "publisher" "publisher" angemeldet
    Wenn ich auf der Wordpress Seite "wp-admin/edit.php" bin
    Und ich mich mit der Maus über dem Element "ein simpler poster title" befinde
    Und ich auf den Link "Trash" klicke
    Dann sollte ich den Text "1 post moved to the Trash." sehen