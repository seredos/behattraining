<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 01:52
 */

namespace App\Repository;


use App\Entity\WpUsers;
use Doctrine\ORM\EntityRepository;
use UnexpectedValueException;

class WpUserRepository extends EntityRepository
{
    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param mixed    $id          The identifier.
     * @param int|null $lockMode    One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     *
     * @return WpUsers|null The entity instance or NULL if the entity can not be found.
     */
    public function find($id, $lockMode = null, $lockVersion = null){
        return parent::find($id,$lockMode,$lockVersion);
    }

    /**
     * Finds all objects in the repository.
     *
     * @return WpUsers[] The objects.
     */
    public function findAll(){
        return parent::findAll();
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param mixed[]       $criteria
     * @param string[]|null $orderBy
     * @param int|null      $limit
     * @param int|null      $offset
     *
     * @return WpUsers[] The objects.
     *
     * @throws UnexpectedValueException
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null){
        return parent::findBy($criteria,$orderBy,$limit,$offset);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return WpUsers|null The entity instance or NULL if the entity can not be found.
     */
    public function findOneBy(array $criteria, array $orderBy = null){
        return parent::findOneBy($criteria);
    }
}