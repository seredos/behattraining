<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:28
 */

namespace App\Fixture;


use App\Entity\WpUsers;
use Hautelook\Phpass\PasswordHash;

abstract class WpUserFixtureBase extends FixtureBase
{
    /**
     * @param string $userLogin
     * @param string $userPassword
     * @param string|null $userNiceName
     * @param string|null $userEmail
     * @param string|null $userUrl
     * @param \DateTime|null $userRegistered
     * @param string|null $userActivationKey
     * @param int $userStatus
     * @param string|null $displayName
     * @throws \Exception
     */
    protected function createUser(string $userLogin, string $userPassword, string $userNiceName = '', string $userEmail = '', string $displayName = '', string $userUrl = '', \DateTime $userRegistered = null, string $userActivationKey = '', $userStatus = 0)
    {
        $user = new WpUsers();
        $pass = new PasswordHash(8, true);

        $userRegistered = $userRegistered ? $userRegistered : new \DateTime();

        $user->setUserLogin($userLogin)
            ->setUserPass($pass->HashPassword($userPassword))
            ->setUserRegistered($userRegistered)
            ->setUserNicename($userNiceName)
            ->setUserEmail($userEmail)
            ->setUserUrl($userUrl)
            ->setUserActivationKey($userActivationKey)
            ->setUserStatus($userStatus)
            ->setDisplayName($displayName);

        $this->createUserByObject($user);
    }

    protected function createUserByObject(WpUsers $user){
        $this->manager->persist($user);
        $this->addReference('wpUser-' . $user->getUserLogin(), $user);
    }
}