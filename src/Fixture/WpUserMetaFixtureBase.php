<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:27
 */

namespace App\Fixture;


use App\Entity\WpUsermeta;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Fixture\WpUserFixture;

abstract class WpUserMetaFixtureBase extends FixtureBase implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [WpUserFixture::class];
    }

    protected function createUserMetaArray(int $userId, array $values){
        foreach ($values as $key => $value){
            $this->createUserMeta($userId,$key,$value);
        }
    }

    protected function createUserMeta(int $userId, string $metaKey, $metaValue = null){
        $userMeta = new WpUsermeta();

        $userMeta->setUserId($userId)
            ->setMetaKey($metaKey)
            ->setMetaValue($metaValue);

        $this->createUserMetaByObject($userMeta);
    }

    protected function createUserMetaByObject(WpUsermeta $userMeta){
        $this->manager->persist($userMeta);
    }
}