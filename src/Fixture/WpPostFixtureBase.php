<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:30
 */

namespace App\Fixture;


use App\Entity\WpPosts;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Fixture\WpUserFixture;

abstract class WpPostFixtureBase extends FixtureBase implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [WpUserFixture::class];
    }

    /**
     * @param int $userId
     * @param string $postName
     * @param string $postTitle
     * @param string $postContent
     * @param string $postType
     * @param string $postStatus
     * @param string $commentStatus
     * @param string $pingStatus
     * @param string $postPassword
     * @param string $postExcerpt
     * @param string $postContentFiltered
     * @param string $postMimeType
     * @param int $commentCount
     * @param int $menuOrder
     * @param \DateTime|null $postDate
     * @param \DateTime|null $postDateGmt
     * @param \DateTime|null $postModified
     * @param \DateTime|null $postModifiedGmt
     * @param string $toPing
     * @param string $pinged
     * @param string $url
     * @throws \Exception
     */
    protected function createPost(int $userId,
                                  string $postName,
                                  string $postTitle,
                                  string $postContent = '',
                                  string $postType = 'page',
                                  string $postStatus = 'publish',
                                  string $commentStatus = 'closed',
                                  string $pingStatus = 'open',
                                  string $postPassword = '',
                                  string $postExcerpt = '',
                                  string $postContentFiltered = '',
                                  string $postMimeType = '',
                                  int $commentCount = 0,
                                  int $menuOrder = 0,
                                  \DateTime $postDate = null,
                                  \DateTime $postDateGmt = null,
                                  \DateTime $postModified = null,
                                  \DateTime $postModifiedGmt = null,
                                  string $toPing = '',
                                  string $pinged = '',
                                  string $url = 'http://127.0.0.1/?page_id=')
    {
        $post = new WpPosts();

        $postDate = $postDate ? $postDate : new \DateTime();
        $postDateGmt = $postDateGmt ? $postDateGmt : $postDate;
        $postModified = $postModified ? $postModified : $postDate;
        $postModifiedGmt = $postModifiedGmt ? $postModifiedGmt : $postModified;

        $post->setPostAuthor($userId)
            ->setPostDate($postDate)
            ->setPostDateGmt($postDateGmt)
            ->setPostContent($postContent)
            ->setPostTitle($postTitle)
            ->setPostExcerpt($postExcerpt)
            ->setPostStatus($postStatus)
            ->setCommentStatus($commentStatus)
            ->setPingStatus($pingStatus)
            ->setPostPassword($postPassword)
            ->setPostName($postName)
            ->setToPing($toPing)
            ->setPinged($pinged)
            ->setPostModified($postModified)
            ->setPostModifiedGmt($postModifiedGmt)
            ->setPostContentFiltered($postContentFiltered)
            ->setMenuOrder($menuOrder)
            ->setPostType($postType)
            ->setPostMimeType($postMimeType)
            ->setCommentCount($commentCount)
            ->setGuid($url);

        $this->createPostByObject($post);
    }

    public function createPostByObject(WpPosts $post){
        $this->manager->persist($post);
        $this->manager->flush();

        $post->setGuid($post->getGuid().$post->getId());
        $this->manager->persist($post);
        $this->addReference('wpPost-'.$post->getPostName(),$post);
    }
}