<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:28
 */

namespace App\Fixture;


use App\Entity\WpTermTaxonomy;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class WpTermTaxonomyFixtureBase extends FixtureBase implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [WpTermFixtureBase::class];
    }

    function createData()
    {
        $this->createTermTaxonomy(
            $this->getReference('wpTerm-Uncategorized')->getId(),
            'category'
        );
    }

    protected function createTermTaxonomy(int $termId, string $taxonomy, string $description = '', int $parent = 0, int $count = 1){
        $termTaxonomy = new WpTermTaxonomy();
        $termTaxonomy->setTermId($termId)
            ->setTaxonomy($taxonomy)
            ->setDescription($description)
            ->setParent($parent)
            ->setCount($count);
        $this->createTermTaxonomyByObject($termTaxonomy);
    }

    protected function createTermTaxonomyByObject(WpTermTaxonomy $termTaxonomy){
        $this->manager->persist($termTaxonomy);
        $this->addReference('wpTermTaxonomy-'.$termTaxonomy->getTaxonomy(),$termTaxonomy);
    }
}