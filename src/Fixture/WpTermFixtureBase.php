<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:29
 */

namespace App\Fixture;



use App\Entity\WpTerms;

class WpTermFixtureBase extends FixtureBase
{
    function createData()
    {
        $this->createTerm('Uncategorized','uncategorized');
    }

    protected function createTerm(string $name, string $slug, int $termGroup = 0){
        $term = new WpTerms();

        $term->setName($name)
            ->setSlug($slug)
            ->setTermGroup($termGroup);

        $this->createTermByObject($term);
    }

    protected function createTermByObject(WpTerms $term){
        $this->manager->persist($term);
        $this->addReference('wpTerm-' . $term->getName(), $term);
    }
}