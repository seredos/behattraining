<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:30
 */

namespace App\Fixture;



use App\Entity\WpOptions;

abstract class WpOptionFixtureBase extends FixtureBase
{
    function createData(){
        $this->createOption('_site_transient_timeout_theme_roots','1549222476','no');
        $this->createOption('_site_transient_theme_roots',['test' => '/themes',
            'twentynineteen' => '/themes',
            'twentyseventeen' => '/themes',
            'twentysixteen' => '/themes'],'no');
        $this->createOption('_site_transient_update_themes',(object)(array(
            'last_checked' => 1549220677,
            'checked' =>
                array (
                    'test' => '1',
                    'twentynineteen' => '1.2',
                    'twentyseventeen' => '2.0',
                    'twentysixteen' => '1.8',
                ),
            'response' =>
                array (
                ),
            'translations' =>
                array (
                ),
        )),'no');
        $this->createOption('_site_transient_update_plugins',(object)(array(
            'last_checked' => 1549220677,
            'checked' =>
                array (
                    'akismet/akismet.php' => '4.1',
                    'hello.php' => '1.7.1',
                    'relative-url/relative-url.php' => '0.1.7',
                ),
            'response' =>
                array (
                    'akismet/akismet.php' =>
                        (object)(array(
                            'id' => 'w.org/plugins/akismet',
                            'slug' => 'akismet',
                            'plugin' => 'akismet/akismet.php',
                            'new_version' => '4.1.1',
                            'url' => 'https://wordpress.org/plugins/akismet/',
                            'package' => 'https://downloads.wordpress.org/plugin/akismet.4.1.1.zip',
                            'icons' =>
                                array (
                                    '2x' => 'https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272',
                                    '1x' => 'https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272',
                                ),
                            'banners' =>
                                array (
                                    '1x' => 'https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904',
                                ),
                            'banners_rtl' =>
                                array (
                                ),
                            'tested' => '5.0.3',
                            'requires_php' => false,
                            'compatibility' =>
                                (object)(array(
                                )),
                        )),
                ),
            'translations' =>
                array (
                ),
            'no_update' =>
                array (
                    'hello.php' =>
                        (object)(array(
                            'id' => 'w.org/plugins/hello-dolly',
                            'slug' => 'hello-dolly',
                            'plugin' => 'hello.php',
                            'new_version' => '1.6',
                            'url' => 'https://wordpress.org/plugins/hello-dolly/',
                            'package' => 'https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip',
                            'icons' =>
                                array (
                                    '2x' => 'https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907',
                                    '1x' => 'https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907',
                                ),
                            'banners' =>
                                array (
                                    '1x' => 'https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342',
                                ),
                            'banners_rtl' =>
                                array (
                                ),
                        )),
                    'relative-url/relative-url.php' =>
                        (object)(array(
                            'id' => 'w.org/plugins/relative-url',
                            'slug' => 'relative-url',
                            'plugin' => 'relative-url/relative-url.php',
                            'new_version' => '0.1.7',
                            'url' => 'https://wordpress.org/plugins/relative-url/',
                            'package' => 'https://downloads.wordpress.org/plugin/relative-url.0.1.7.zip',
                            'icons' =>
                                array (
                                    '2x' => 'https://ps.w.org/relative-url/assets/icon-256x256.png?rev=1568302',
                                    '1x' => 'https://ps.w.org/relative-url/assets/icon.svg?rev=1568302',
                                    'svg' => 'https://ps.w.org/relative-url/assets/icon.svg?rev=1568302',
                                ),
                            'banners' =>
                                array (
                                    '2x' => 'https://ps.w.org/relative-url/assets/banner-1544x500.png?rev=1568302',
                                    '1x' => 'https://ps.w.org/relative-url/assets/banner-772x250.png?rev=1568302',
                                ),
                            'banners_rtl' =>
                                array (
                                ),
                        )),
                ),
        )),'no');

        $this->createOptionByArray([
            'widget_pages' => [
                '_multiwidget' => 1
            ],
            'widget_calendar' => [
                '_multiwidget' => 1
            ],
            'widget_archives' => [
                '_multiwidget' => 1
            ],
            'widget_links' => [
                '_multiwidget' => 1
            ],
            'widget_media_audio' => [
                '_multiwidget' => 1
            ],
            'widget_media_image' => [
                '_multiwidget' => 1
            ],
            'widget_media_gallery' => [
                '_multiwidget' => 1
            ],
            'widget_media_video' => [
                '_multiwidget' => 1
            ],
            'widget_meta' => [
                '_multiwidget' => 1
            ],
            'widget_search' => [
                '_multiwidget' => 1
            ],
            'widget_text' => [
                '_multiwidget' => 1
            ],
            'widget_categories' => [
                '_multiwidget' => 1
            ],
            'widget_rss' => [
                '_multiwidget' => 1
            ],
            'widget_tag_cloud' => [
                '_multiwidget' => 1
            ],
            'widget_nav_menu' => [
                '_multiwidget' => 1
            ],
            'widget_custom_html' => [
                '_multiwidget' => 1
            ],
            'cron' => [
                1549224270 =>
                    array (
                        'wp_privacy_delete_old_export_files' =>
                            array (
                                '40cd750bba9870f18aada2478b24840a' =>
                                    array (
                                        'schedule' => 'hourly',
                                        'args' =>
                                            array (
                                            ),
                                        'interval' => 3600,
                                    ),
                            ),
                    ),
                1549263870 =>
                    array (
                        'wp_version_check' =>
                            array (
                                '40cd750bba9870f18aada2478b24840a' =>
                                    array (
                                        'schedule' => 'twicedaily',
                                        'args' =>
                                            array (
                                            ),
                                        'interval' => 43200,
                                    ),
                            ),
                        'wp_update_plugins' =>
                            array (
                                '40cd750bba9870f18aada2478b24840a' =>
                                    array (
                                        'schedule' => 'twicedaily',
                                        'args' =>
                                            array (
                                            ),
                                        'interval' => 43200,
                                    ),
                            ),
                        'wp_update_themes' =>
                            array (
                                '40cd750bba9870f18aada2478b24840a' =>
                                    array (
                                        'schedule' => 'twicedaily',
                                        'args' =>
                                            array (
                                            ),
                                        'interval' => 43200,
                                    ),
                            ),
                    ),
                'version' => 2,
            ]
        ]);

        $this->createOption('moderation_keys','','no');
        $this->createOption('recently_edited','','no');
        $this->createOption('blacklist_keys','','no');
        $this->createOption('uninstall_plugins',[],'no');
    }

    protected function createOptionByObject(WpOptions $option){
        $this->manager->persist($option);
    }

    protected function createOption(string $optionName, $optionValue, string $autoload = 'yes'){
        $option = new WpOptions();
        $option->setOptionName($optionName)
            ->setOptionValue($optionValue)
            ->setAutoload($autoload);

        $this->createOptionByObject($option);
    }

    protected function createOptionByArray(array $options){
        foreach ($options as $key => $option){
            $this->createOption($key,$option);
        }
    }
}