<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:44
 */

namespace App\Fixture;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

abstract class FixtureBase extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->createData();
        $manager->flush();
    }

    /**
     * @throws \Exception
     */
    abstract function createData();
}