<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 17:29
 */

namespace App\Fixture;



use App\Entity\WpTermRelationships;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Fixture\WpPostFixture;

abstract class WpTermRelationshipFixtureBase extends FixtureBase implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [WpPostFixture::class,WpLinkFixtureBase::class,WpTermTaxonomyFixtureBase::class];
    }

    function createData()
    {
        $this->createTermRelationship(
            $this->getReference('wpPost-hello-world')->getId(),
            $this->getReference('wpTermTaxonomy-category')->getId()
        );
    }

    protected function createTermRelationship(int $objectId, int $termTaxonomyId, int $termOrder = 0){
        $termRelationship = new WpTermRelationships();
        $termRelationship->setTermOrder($termOrder)
            ->setObjectId($objectId)
            ->setTermTaxonomyId($termTaxonomyId);
        $this->createTermRelationshipByObject($termRelationship);
    }

    protected function createTermRelationshipByObject(WpTermRelationships $termRelationship){
        $this->manager->persist($termRelationship);
    }
}