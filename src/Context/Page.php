<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 03:41
 */

namespace App\Context;


use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Exception\UnsupportedDriverActionException;

class Page
{
    private $context;

    public function __construct(BaseContext $context)
    {
        $this->context = $context;
    }

    public function visit($url){
        $this->waitForLoadedPage();
        $this->context->getSession()->visit($url);
    }

    /**
     * @param $selector
     * @param $locator
     * @return \Behat\Mink\Element\NodeElement[]
     */
    public function findAll($selector, $locator){
        return $this->context->getSession()->getPage()->findAll($selector,$locator);
    }

    /**
     * @param $selector
     * @param $locator
     * @return \Behat\Mink\Element\NodeElement|mixed|null
     */
    public function find($selector, $locator){
        return $this->context->getSession()->getPage()->find($selector,$locator);
    }

    /**
     * @return string
     */
    public function getContent(){
        return $this->context->getSession()->getPage()->getContent();
    }

    /**
     * @return string
     */
    public function getCurrentUrl(){
        return $this->context->getSession()->getCurrentUrl();
    }

    /**
     * @return string
     */
    public function getHtml(){
        return $this->context->getSession()->getPage()->getHtml();
    }

    /**
     * @return string
     */
    public function getOuterHtml(){
        return $this->context->getSession()->getPage()->getOuterHtml();
    }

    /**
     * @return string
     */
    public function getText(){
        return $this->context->getSession()->getPage()->getOuterHtml();
    }

    /**
     * @param $selector
     * @param $locator
     * @return bool
     */
    public function has($selector,$locator){
        return $this->context->getSession()->getPage()->has($selector,$locator);
    }

    /**
     * @param $timout
     * @param $callback
     * @return mixed
     */
    public function waitFor($timout, $callback){
        return $this->context->getSession()->getPage()->waitFor($timout, $callback);
    }

    /**
     * @param $locator
     */
    public function waitForLink($locator){
        $this->waitFor(100,function (DocumentElement $page) use ($locator){
            return $page->findLink($locator) !== null;
        });
    }

    /**
     * @param $locator
     */
    public function waitForButton($locator){
        $this->waitFor(100,function (DocumentElement $page) use ($locator){
            return $page->findButton($locator) !== null;
        });
    }

    /**
     * @param $locator
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function checkField($locator){
        return $this->context->getSession()->getPage()->checkField($locator);
    }

    /**
     * @param $locator
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function clickLink($locator){
        $this->context->getSession()->getPage()->clickLink($locator);
    }

    /**
     * @param $locator
     * @param $value
     */
    public function fillField($locator,$value){
        $this->waitFor(100,function (DocumentElement $page) use ($locator, $value){
            $page->fillField($locator,$value);
            return $page->findField($locator)->getValue() === $value;
        });
    }

    /**
     * @param $locator
     * @return \Behat\Mink\Element\NodeElement|null
     */
    public function findButton($locator){
        return $this->context->getSession()->getPage()->findButton($locator);
    }

    /**
     * @param $id
     * @return \Behat\Mink\Element\NodeElement|null
     */
    public function findById($id){
        return $this->context->getSession()->getPage()->findById($id);
    }

    /**
     * @param $locator
     * @return \Behat\Mink\Element\NodeElement|null
     */
    public function findField($locator){
        return $this->context->getSession()->getPage()->findField($locator);
    }

    /**
     * @param $locator
     * @return \Behat\Mink\Element\NodeElement|null
     */
    public function findLink($locator){
        return $this->context->getSession()->getPage()->findLink($locator);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasButton($locator){
        return $this->context->getSession()->getPage()->hasButton($locator);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasCheckedField($locator){
        return $this->context->getSession()->getPage()->hasCheckedField($locator);
    }

    /**
     * @param $content
     * @return bool
     */
    public function hasContent($content){
        return $this->context->getSession()->getPage()->hasContent($content);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasField($locator){
        return $this->context->getSession()->getPage()->hasField($locator);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasLink($locator){
        return $this->context->getSession()->getPage()->hasLink($locator);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasSelect($locator){
        return $this->context->getSession()->getPage()->hasSelect($locator);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasTable($locator){
        return $this->context->getSession()->getPage()->hasTable($locator);
    }

    /**
     * @param $locator
     * @return bool
     */
    public function hasUncheckedField($locator){
        return $this->context->getSession()->getPage()->hasUncheckedField($locator);
    }

    /**
     * @param $locator
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function pressButton($locator){
        $this->context->getSession()->getPage()->pressButton($locator);
    }

    /**
     * @param $locator
     * @param $value
     * @param bool $multiple
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function selectFieldOption($locator, $value, $multiple = false){
        $this->context->getSession()->getPage()->selectFieldOption($locator,$value,$multiple);
    }

    /**
     * @param $locator
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function uncheckField($locator){
        $this->context->getSession()->getPage()->uncheckField($locator);
    }

    public function takeScreenshot(){
        $directory = $this->context->getKernel()->getProjectDir().'/logs/';
        $stack = debug_backtrace();
        $name = uniqid() . '_' . $stack[1]['function'];
        try {
            $name .= '.png';
            $img = $this->context->getSession()->getScreenshot();
            file_put_contents($directory . $name, $img);
        }catch (UnsupportedDriverActionException $e){
            $name .= ".html";
            $content = $this->context->getSession()->getPage()->getContent();
            file_put_contents($directory . $name, $content);
        }
        print_r('[DEBUG] saved data: logs/'.$name);
    }

    public function waitForLoadedPage(){
        if ($this->context->getSession()
                ->getDriver() instanceof Selenium2Driver
        ) {
            $this->context->getSession()->wait(1000, '(typeof window.jQuery == "function" && typeof jQuery != "undefined" && 0 === jQuery.active) || document.readyState === "complete"');
        }
    }
}