<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 02:07
 */

namespace App\Context;


use Behat\MinkExtension\Context\RawMinkContext;
use Hautelook\Phpass\PasswordHash;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class BaseContext extends RawMinkContext
{
    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var Database
     */
    protected $database;
    /**
     * @var Page
     */
    protected $page;

    /**
     * @var PasswordHash
     */
    private $pass;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->database = new Database($this->kernel);
        $this->page = new Page($this);
        $this->pass = new PasswordHash(8,true);
    }

    /**
     * @return string
     */
    protected function getBaseUrl(){
        return $this->getMinkParameter('base_url');
    }

    /**
     * @param $password
     * @param $hash
     * @return bool
     */
    protected function checkPassword($password, $hash){
        return $this->pass->CheckPassword($password,$hash);
    }

    /**
     * @return KernelInterface
     */
    public function getKernel(){
        return $this->kernel;
    }

    public function assertEquals($expected, $actual, string $message = ''){
        TestCase::assertEquals($expected, $actual, $message);
    }

    public function assertNotEquals($expected, $actual, string $message = ''){
        TestCase::assertNotEquals($expected, $actual, $message);
    }

    public function assertContains($needle, $haystack, string $message = ''){
        TestCase::assertContains($needle, $haystack, $message);
    }

    public function assertNotContains($needle, $haystack, string $message = ''){
        TestCase::assertNotContains($needle, $haystack, $message);
    }

    public function assertNull($actual, string $message = ''){
        TestCase::assertNull($actual, $message);
    }

    public function assertNotNull($actual, string $message = ''){
        TestCase::assertNotNull($actual,$message);
    }

    public function assertTrue($actual, string $message = ''){
        TestCase::assertTrue($actual,$message);
    }

    public function assertFalse($actual, string $message = ''){
        TestCase::assertFalse($actual,$message);
    }
}