<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 02:06
 */

namespace App\Context;


use App\Entity\WpOptions;
use App\Entity\WpPosts;
use App\Entity\WpUsermeta;
use App\Entity\WpUsers;
use App\Repository\WpOptionRepository;
use App\Repository\WpPostRepository;
use App\Repository\WpUserMetaRepository;
use App\Repository\WpUserRepository;
use Symfony\Component\HttpKernel\KernelInterface;

class Database
{
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @return WpUserRepository
     */
    public function getWpUserRepository()
    {
        return $this->kernel->getContainer()
            ->get('doctrine')
            ->getManager()->getRepository(WpUsers::class);
    }

    /**
     * @return WpUserMetaRepository
     */
    public function getWpUserMetaRepository()
    {
        return $this->kernel->getContainer()
            ->get('doctrine')
            ->getManager()->getRepository(WpUsermeta::class);
    }

    /**
     * @return WpOptionRepository
     */
    public function getWpOptionRepository()
    {
        return $this->kernel->getContainer()
            ->get('doctrine')
            ->getManager()->getRepository(WpOptions::class);
    }

    /**
     * @return WpPostRepository
     */
    public function getWpPostRepository(){
        return $this->kernel->getContainer()
            ->get('doctrine')->getManager()->getRepository(WpPosts::class);
    }
}