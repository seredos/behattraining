<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 03.02.19
 * Time: 04:41
 */

namespace App\Context;


use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Mink\Driver\Selenium2Driver;

class DebugContext extends BaseContext
{
    const TAG_SCREEN_1680_1050 = 'screen_1680x1050';

    /**
     * @BeforeScenario
     *
     * @param BeforeScenarioScope $event
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function beforeScenario (BeforeScenarioScope $event) {
        if ($this->getSession()
                ->getDriver() instanceof Selenium2Driver
        ) {
            if ($event->getScenario()
                ->hasTag(self::TAG_SCREEN_1680_1050)
            ) {
                $this->getSession()
                    ->getDriver()
                    ->resizeWindow(1680, 1050);
            } else {
                $this->getSession()
                    ->getDriver()
                    ->resizeWindow(1024, 768);
            }
        }
    }

    /**
     * @AfterStep
     * @param AfterStepScope $event
     */
    public function afterStep(AfterStepScope $event){
        $this->page->waitForLoadedPage();
    }

    /**
     * @Given /^ich habe einen Screenshot gemacht$/
     */
    public function GivenICreateScreenshot(){
        $this->page->takeScreenshot();
    }

    /**
     * @When /^ich einen Screenshot mache$/
     */
    public function WhenICreateScreenshot(){
        $this->page->takeScreenshot();
    }

    /**
     * @Then /^werde ich einen Screenshot machen$/
     */
    public function ThenIWillCreateScreenshot(){
        $this->page->takeScreenshot();
    }

    /**
     * @Then /^sollte ich einen Screenshot machen$/
     */
    public function ThenIShouldCreateScreenshot(){
        $this->page->takeScreenshot();
    }

    /**
     * @When /^ich warte "([^"]*)" Sekunde$/
     */
    public function iWaitSecond($sleep)
    {
        sleep($sleep);
    }

    /**
     * @When /^ich warte "([^"]*)" Sekunden$/
     */
    public function iWaitSeconds($sleep)
    {
        $this->iWaitSecond($sleep);
    }

    /**
     * @When /^ich "([^"]*)" Sekunde warte$/
     */
    public function WaitISecond($sleep)
    {
        $this->iWaitSecond($sleep);
    }

    /**
     * @When /^ich "([^"]*)" Sekunden warte$/
     */
    public function WaitISeconds($sleep)
    {
        $this->iWaitSecond($sleep);
    }
}