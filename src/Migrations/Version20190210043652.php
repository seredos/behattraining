<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190210043652 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wp_links (link_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, link_url VARCHAR(255) NOT NULL, link_name VARCHAR(255) NOT NULL, link_image VARCHAR(255) NOT NULL, link_target VARCHAR(25) NOT NULL, link_description VARCHAR(255) NOT NULL, link_visible VARCHAR(20) DEFAULT \'Y\' NOT NULL, link_owner BIGINT UNSIGNED DEFAULT 1 NOT NULL, link_rating INT NOT NULL, link_updated DATETIME NOT NULL, link_rel VARCHAR(255) NOT NULL, link_notes MEDIUMTEXT NOT NULL, link_rss VARCHAR(255) NOT NULL, INDEX link_visible (link_visible), PRIMARY KEY(link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_posts (ID BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, post_author BIGINT UNSIGNED NOT NULL, post_date DATETIME NOT NULL, post_date_gmt DATETIME NOT NULL, post_content LONGTEXT NOT NULL, post_title TEXT NOT NULL, post_excerpt TEXT NOT NULL, post_status VARCHAR(20) DEFAULT \'publish\' NOT NULL, comment_status VARCHAR(20) DEFAULT \'open\' NOT NULL, ping_status VARCHAR(20) DEFAULT \'open\' NOT NULL, post_password VARCHAR(255) NOT NULL, post_name VARCHAR(200) NOT NULL, to_ping TEXT NOT NULL, pinged TEXT NOT NULL, post_modified DATETIME NOT NULL, post_modified_gmt DATETIME NOT NULL, post_content_filtered LONGTEXT NOT NULL, post_parent BIGINT UNSIGNED NOT NULL, guid VARCHAR(255) NOT NULL, menu_order INT NOT NULL, post_type VARCHAR(20) DEFAULT \'post\' NOT NULL, post_mime_type VARCHAR(100) NOT NULL, comment_count BIGINT NOT NULL, INDEX type_status_date (post_type, post_status, post_date, ID), INDEX post_parent (post_parent), INDEX post_author (post_author), INDEX post_name (post_name), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_options (option_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, option_name VARCHAR(191) NOT NULL, option_value LONGTEXT NOT NULL, autoload VARCHAR(20) DEFAULT \'yes\' NOT NULL, UNIQUE INDEX option_name (option_name), PRIMARY KEY(option_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_comments (comment_ID BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, comment_post_ID BIGINT UNSIGNED NOT NULL, comment_author TINYTEXT NOT NULL, comment_author_email VARCHAR(100) NOT NULL, comment_author_url VARCHAR(200) NOT NULL, comment_author_IP VARCHAR(100) NOT NULL, comment_date DATETIME NOT NULL, comment_date_gmt DATETIME NOT NULL, comment_content TEXT NOT NULL, comment_karma INT NOT NULL, comment_approved VARCHAR(20) DEFAULT \'1\' NOT NULL, comment_agent VARCHAR(255) NOT NULL, comment_type VARCHAR(20) NOT NULL, comment_parent BIGINT UNSIGNED NOT NULL, user_id BIGINT UNSIGNED NOT NULL, INDEX comment_approved_date_gmt (comment_approved, comment_date_gmt), INDEX comment_date_gmt (comment_date_gmt), INDEX comment_author_email (comment_author_email), INDEX comment_parent (comment_parent), INDEX comment_post_ID (comment_post_ID), PRIMARY KEY(comment_ID)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_terms (term_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(200) NOT NULL, slug VARCHAR(200) NOT NULL, term_group BIGINT NOT NULL, INDEX name (name), INDEX slug (slug), PRIMARY KEY(term_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_term_taxonomy (term_taxonomy_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, term_id BIGINT UNSIGNED NOT NULL, taxonomy VARCHAR(32) NOT NULL, description LONGTEXT NOT NULL, parent BIGINT UNSIGNED NOT NULL, count BIGINT NOT NULL, INDEX taxonomy (taxonomy), UNIQUE INDEX term_id_taxonomy (term_id, taxonomy), PRIMARY KEY(term_taxonomy_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_termmeta (meta_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, term_id BIGINT UNSIGNED NOT NULL, meta_key VARCHAR(255) DEFAULT NULL, meta_value LONGTEXT DEFAULT NULL, INDEX meta_key (meta_key), INDEX term_id (term_id), PRIMARY KEY(meta_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_postmeta (meta_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, post_id BIGINT UNSIGNED NOT NULL, meta_key VARCHAR(255) DEFAULT NULL, meta_value LONGTEXT DEFAULT NULL, INDEX meta_key (meta_key), INDEX post_id (post_id), PRIMARY KEY(meta_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_usermeta (umeta_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id BIGINT UNSIGNED NOT NULL, meta_key VARCHAR(255) DEFAULT NULL, meta_value LONGTEXT DEFAULT NULL, INDEX meta_key (meta_key), INDEX user_id (user_id), PRIMARY KEY(umeta_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_users (ID BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, user_login VARCHAR(60) NOT NULL, user_pass VARCHAR(255) NOT NULL, user_nicename VARCHAR(50) NOT NULL, user_email VARCHAR(100) NOT NULL, user_url VARCHAR(100) NOT NULL, user_registered DATETIME NOT NULL, user_activation_key VARCHAR(255) NOT NULL, user_status INT NOT NULL, display_name VARCHAR(250) NOT NULL, INDEX user_nicename (user_nicename), INDEX user_email (user_email), INDEX user_login_key (user_login), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_term_relationships (object_id BIGINT UNSIGNED NOT NULL, term_taxonomy_id BIGINT UNSIGNED NOT NULL, term_order INT NOT NULL, INDEX term_taxonomy_id (term_taxonomy_id), PRIMARY KEY(object_id, term_taxonomy_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wp_commentmeta (meta_id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL, comment_id BIGINT UNSIGNED NOT NULL, meta_key VARCHAR(255) DEFAULT NULL, meta_value LONGTEXT DEFAULT NULL, INDEX meta_key (meta_key), INDEX comment_id (comment_id), PRIMARY KEY(meta_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE wp_links');
        $this->addSql('DROP TABLE wp_posts');
        $this->addSql('DROP TABLE wp_options');
        $this->addSql('DROP TABLE wp_comments');
        $this->addSql('DROP TABLE wp_terms');
        $this->addSql('DROP TABLE wp_term_taxonomy');
        $this->addSql('DROP TABLE wp_termmeta');
        $this->addSql('DROP TABLE wp_postmeta');
        $this->addSql('DROP TABLE wp_usermeta');
        $this->addSql('DROP TABLE wp_users');
        $this->addSql('DROP TABLE wp_term_relationships');
        $this->addSql('DROP TABLE wp_commentmeta');
    }
}
