FROM bitnami/wordpress:5.0.3 AS stage

COPY bin/relative-url/relative-url.php /usr/src/wordpress/wp-content/plugins/

RUN curl --silent --show-error https://getcomposer.org/installer | php && \
    mv composer.phar /usr/bin/composer

WORKDIR /builds/aappen/behat-training/
ENV PATH "/builds/aappen/behat-training/:$PATH"