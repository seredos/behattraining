Behat Training
==============

Dieses Projekt dient zum sammeln von Erfahrungen mit dem schreiben von Behat-Tests

Installation
------------

> Diese Schritte werden nur benötigt, wenn das Projekt auf einem neuen Computer ausgecheckt wird.
> Die Installation sollte vor der Schulung bereits stattgefunden haben

#### Das Projekt auschecken
```bash
git clone https://gitlab.com/seredos/behattraining.git
cd behat-training
```

#### Projekt builden
```bash
docker-compose build
docker-compose up -d
docker-compose exec wordpress behat build
docker-compose exec wordpress behat delete-db
docker-compose exec wordpress behat reset
docker-compose stop
```
Die aufgeführten Befehle führen dazu, dass alle benötigten Abhängigkeiten auf dem Rechner installiert werden.

Erste Schritte
--------------

Um das System zu starten muss der folgende Befehl ausgeführt werden
```bash
docker-compose up -d
```
Der Befehl startet alle Container, die für das ausführen der Tests benötigt werden.

Zum beenden der Umgebung muss der folgende Befehl ausgeführt werden:

```bash
docker-compose stop
```

Die folgenden Container werden dadurch gestartet

| Container    | URL                                | Information                                                    |
|--------------|------------------------------------|----------------------------------------------------------------|
| wordpress    | http://127.0.0.1/                  | Wordpress Website                                              |
| phpmyadmin   | http://127.0.0.1:8080/             | Datenbank Website                                              |
| selenium-hub | http://127.0.0.1:4444/grid/console | Schnittstelle für die Kommunikation mit verschiedenen Browsern |
| mysql        |                                    | Datenbank                                                      |
| firefox      |                                    | Firefox Browser (Selenium)                                     |
| chrome       |                                    | Chrome Browser (Selenium)                                      |

Die folgenden Befehle können entweder innerhalb des Containers ausgeführt werden
```bash
$ docker-compose exec wordpress bash
$ behat validate
```
oder als einzelner Befehl an den Container gesendet werden
```bash
$ docker-compose exec wordpress behat validate
```

| Command                                | Information                                                                     |
|----------------------------------------|---------------------------------------------------------------------------------|
| ```behat reset```                      | Setzt die Datenbank auf Fixture-Datenbestand zurück                             |
| ```behat tests --profile firefox```    | führt alle Tests aus - Selenium Tests werden mittels Firefox ausgeführt         |
| ```behat all-tests```                  | führt alle Tests aus - Seleniumtests werden unter Chrome und Firefox ausgeführt |
| ```behat features/gherkin.feature:8``` | führt das Szenario an Zeile 7 aus                                               |
| ```behat validate```                   | Überprüft, ob alle Daten für die Testrail Synchronization vorhanden sind        |