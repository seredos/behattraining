#!/usr/bin/env bash

SCRIPT=$(readlink -f "$0")
BINDIR=$(dirname "$SCRIPT")
PROJECTDIR=$(dirname "$BINDIR")

docker build --target=build -t seretos/behat-training ${PROJECTDIR}/
docker tag seretos/behat-training seretos/behat-training:0.0.1
docker push seretos/behat-training:0.0.1