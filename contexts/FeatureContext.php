<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 05.02.19
 * Time: 23:16
 */

namespace Context;


use App\Context\BaseContext;
use Behat\Mink\Element\NodeElement;

class FeatureContext extends BaseContext
{
    /**
     * @Given /^ich bin auf der Seite "([^"]*)"$/
     */
    public function GivenIVisitPage($page){
        $this->page->visit($page);
    }

    /**
     * @When /^ich auf der Seite "([^"]*)" bin$/
     */
    public function WhenIVisitPage($page){
        $this->page->visit($page);
    }

    /**
     * @When /^ich in das Feld "([^"]*)" den Wert "([^"]*)" eingebe$/
     */
    public function ichInDasFeldDenWertEingebe($name, $value)
    {
        $this->page->fillField($name,$value);
    }

    /**
     * @When /^ich auf den Button "([^"]*)" klicke$/
     */
    public function ichAufDenButtonKlicke($title)
    {
        $this->page->findButton($title)->click();
    }

    /**
     * @When /^sollte ich den Text "([^"]*)" sehen$/
     */
    public function sollteIchDenTextSehen($content)
    {
        $this->assertContains($content,$this->page->getText());
    }

    /**
     * @When /^ich auf der Wordpress Seite "([^"]*)" bin$/
     */
    public function ichAufDerWordpressSeiteBin($page)
    {
        $this->page->visit($this->getBaseUrl().$page);
    }

    /**
     * @When /^ich habe mich mit "([^"]*)" "([^"]*)" angemeldet$/
     */
    public function ichHabeMichMitAngemeldet($username, $password)
    {
        $this->ichAufDerWordpressSeiteBin('wp-login.php');
        $this->ichInDasFeldDenWertEingebe('Username or Email Address',$username);
        $this->ichInDasFeldDenWertEingebe('Password',$password);
        $this->ichAufDenButtonKlicke('Log In');
    }

    /**
     * @When /^ich auf den Link "([^"]*)" klicke$/
     */
    public function ichAufDenLinkKlicke($title)
    {
        $this->page->waitForLink($title);
        $this->page->findLink($title)->click();
    }

    /**
     * @When /^ich das Menü "([^"]*)" öffne$/
     */
    public function ichDasMenueOeffne($title)
    {
        $this->page->findLink($title)->mouseOver();
    }

    /**
     * @When /^ich sollte den Text "([^"]*)" sehen$/
     */
    public function ichSollteDenTextSehen($arg1)
    {
        $this->sollteIchDenTextSehen($arg1);
    }

    /**
     * @When /^sollte ich den Text "([^"]*)" nicht sehen$/
     */
    public function sollteIchDenTextNichtSehen($content)
    {
        $this->assertNotContains($content,$this->page->getText());
    }

    /**
     * @When /^ich den Post "([^"]*)" zur Bearbeitung öffne$/
     */
    public function ichDenPostZurBearbeitungOeffne($title)
    {
        $post = $this->database->getWpPostRepository()->findOneBy(['postTitle' => $title,'postStatus' => 'pending']);
        $this->assertNotNull($post);
        $this->page->visit($this->getBaseUrl().'wp-admin/post.php?post='.$post->getId().'&action=edit');
    }

    /**
     * @When /^ich in dem Feld "([^"]*)" den Wert "([^"]*)" sehe$/
     */
    public function ichInDemFeldDenWertSehe($field, $value)
    {
        $this->assertEquals($value,$this->page->findField($field)->getValue());
    }

    /**
     * @When /^ich mich mit der Maus über dem Element "([^"]*)" befinde$/
     */
    public function ichMichMitDerMausUeberDemElementBefinde($content)
    {
        $this->page->find('named',['content',$content])->mouseOver();
    }

    /**
     * @When /^sehe ich "([^"]*)" als ersten Post$/
     */
    public function seheIchAlsErstenPost($title)
    {
        /* @var $articles NodeElement[]*/
        $articles = $this->page->findById('primary')->findAll('css','article');
        $this->assertTrue(count($articles) > 0);
        $header = $articles[0]->find('css','header');
        $this->assertNotNull($header);
        $this->assertEquals($title,$header->getText());
    }

    /**
     * @When /^ich sehe mehrere Posts$/
     */
    public function ichSeheNebenWeiterePosts()
    {
        /* @var $articles NodeElement[]*/
        $articles = $this->page->findById('primary')->findAll('css','article');
        $this->assertTrue(count($articles) > 1);
    }

    /**
     * @When /^ich sehe, dass der Post "([^"]*)" den Inhalt "([^"]*)" hat$/
     */
    public function ichSeheDassDerPostDenInhaltHat($title, $content)
    {
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title],['postDate' => 'DESC']);
        $article = $this->page->findById('post-'.$post[0]->getId());
        $body = $article->find('css','div.entry-content');
        $this->assertEquals($content,$body->getText());
    }

    /**
     * @When /^ich sehe, dass der Post "([^"]*)" von dem Benutzer "([^"]*)" erstellt wurde$/
     */
    public function ichSeheDassDerPostVonDemBenutzerErstelltWurde($title, $user)
    {
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title],['postDate' => 'DESC']);
        $article = $this->page->findById('post-'.$post[0]->getId());
        $this->assertEquals($user, $article->find('css','span.vcard')->getText());
    }

    /**
     * @When /^ich sehe, dass der Post "([^"]*)" heute erstellt wurde$/
     * @throws \Exception
     */
    public function ichSeheDassDerPostHeuteErstelltWurde($title)
    {
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title],['postDate' => 'DESC']);
        $article = $this->page->findById('post-'.$post[0]->getId());
        $this->assertEquals((new \DateTime())->format('F j, Y'), $article->find('css','span.posted-on')->getText());
    }
}