<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 09.02.19
 * Time: 23:54
 */

namespace Context;


use App\Context\BaseContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;


class GherkinContext extends BaseContext
{
    /**
     * @When /^ich bin auf der Welt$/
     */
    public function iExists()
    {
        print_r('[DEBUG] Dieser Step wird vor jedem Szenario in der gherkin.feature Datei ausgeführt!');
    }

    /**
     * @When /^ich "([^"]*)" sage$/
     */
    public function iSay($message)
    {
        print_r("[DEBUG] ich sage ".$message);
    }

    /**
     * @When /^sehe ich "([^"]*)"$/
     */
    public function iSee($message)
    {
        print_r("[DEBUG] ich sehe ".$message);
    }

    /**
     * @When /^ich hallo Computer sage$/
     */
    public function hiComputer()
    {
        $this->iSay('hallo Computer');
    }

    /**
     * @When /^sehe ich hallo User$/
     */
    public function iSeeHelloUser()
    {
        $this->iSee('hallo User');
    }

    /**
     * @When /^ich hallo Universum sage$/
     */
    public function iSayHelloUniverse()
    {
        $this->iSay('hallo Universum');
    }

    /**
     * @When /^sehe ich hallo Wurm$/
     */
    public function iSeeHelloWorm()
    {
        $this->iSee('hallo Wurm');
    }

    /**
     * @When /^ich sehe die Planeten "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)" und "([^"]*)"$/
     */
    public function iSeePlanets($planet1, $planet2, $planet3, $planet4, $planet5, $planet6, $planet7, $planet8)
    {
        print_r('[DEBUG] ich sehe den Planeten '.$planet1.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet2.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet3.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet4.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet5.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet6.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet7.PHP_EOL);
        print_r('[DEBUG] ich sehe den Planeten '.$planet8.PHP_EOL);
    }

    /**
     * @When /^ich sehe die folgenden Planeten$/
     */
    public function iSeeTheFollowingPlanets(TableNode $table)
    {
        $planets = $table->getColumnsHash();

        foreach($planets as $planet){
            print_r('[DEBUG] ich sehe den Planeten '.$planet['Name'].' ('.$planet['Gravity'].')'.PHP_EOL);
        }
    }

    /**
     * @When /^ich sehe den folgenden Text$/
     */
    public function iSeeTheFollowingText(PyStringNode $string)
    {
        print_r($string->getRaw());
    }
}