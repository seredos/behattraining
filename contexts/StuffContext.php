<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 10.02.19
 * Time: 19:44
 */

namespace Context;


use App\Context\BaseContext;
use Behat\Gherkin\Node\PyStringNode;
use PHPUnit\Framework\TestCase;

class StuffContext extends BaseContext
{
    private $text;
    /**
     * @When /^ich den folgenden Reim sage$/
     */
    public function ichDenFolgendenReimSage(PyStringNode $string)
    {
        $this->text = $string->getRaw();
    }

    /**
     * @When /^habe ich den folgenden Reim gesagt$/
     */
    public function habeIchDenFolgendenReimGesagt(PyStringNode $string)
    {
        TestCase::assertEquals($string->getRaw(),$this->text);
    }
}