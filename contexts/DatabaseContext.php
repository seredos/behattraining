<?php
/**
 * Created by PhpStorm.
 * User: seredos
 * Date: 11.02.19
 * Time: 18:25
 */

namespace Context;


use App\Context\BaseContext;

class DatabaseContext extends BaseContext
{

    /**
     * @When /^der Benutzer "([^"]*)" existiert$/
     */
    public function userExists($userName)
    {
        $user = $this->database->getWpUserRepository()->findOneBy(['userLogin' => $userName]);
        $this->assertNotNull($user,'user not exists');
    }

    /**
     * @When /^der Benutzer "([^"]*)" hat die Rolle "([^"]*)"$/
     */
    public function userHasRole($userName, $roleName)
    {
        $user = $this->database->getWpUserRepository()->findOneBy(['userLogin' => $userName]);
        $userMeta = $this->database->getWpUserMetaRepository()->findOneBy(['userId' => $user->getId(),'metaKey' => 'wp_capabilities']);
        $role = unserialize($userMeta->getMetaValue());

        $this->assertTrue($role[$roleName] === 1);
    }

    /**
     * @When /^die Rolle "([^"]*)" hat die Berechtigung "([^"]*)"$/
     */
    public function roleHasPermission($roleName, $capabilityName)
    {
        $userRoles = unserialize($this->database->getWpOptionRepository()->findOneBy(['optionName' => 'wp_user_roles'])->getOptionValue());
        $this->assertTrue(array_key_exists($capabilityName,
                                        $userRoles[$roleName]['capabilities']),
            'permission does not exists');
    }

    /**
     * @When /^der Benutzer "([^"]*)" hat die Tips für seine Session deaktiviert$/
     */
    public function deactivateUserTips($userName)
    {
        $user = $this->database->getWpUserRepository()->findOneBy(['userLogin' => $userName]);
        $this->getSession()->executeScript('localStorage.WP_DATA_USER_'.$user->getId().'=\'{"core/nux":{"preferences":{"areTipsEnabled":false,"dismissedTips":{}}},"core/edit-post":{"preferences":{"isGeneralSidebarDismissed":false,"panels":{"post-status":{"opened":true}},"features":{"fixedToolbar":false},"editorMode":"visual","pinnedPluginItems":{}}},"core/editor":{"preferences":{"insertUsage":{},"isPublishSidebarEnabled":true}}}\';');
    }

    /**
     * @When /^für den Benutzer "([^"]*)" ist der Visual Editor deaktiviert$/
     */
    public function fuerDenBenutzerIstDerVisualEditorDeaktiviert($userName)
    {
        $user = $this->database->getWpUserRepository()->findOneBy(['userLogin' => $userName]);
        $userMeta = $this->database->getWpUserMetaRepository()->findOneBy(['userId' => $user->getId(),'metaKey' => 'rich_editing']);

        $this->assertEquals($userMeta->getMetaValue(),'false');
    }

    /**
     * @When /^der Post "([^"]*)" ist der neueste Post$/
     */
    public function derPostIstDerNeuestePost($title)
    {
        $post = $this->database->getWpPostRepository()->findBy([],['postDate' => 'DESC'],1);
        $this->assertEquals($title, $post[0]->getPostTitle());
    }

    /**
     * @When /^der Post "([^"]*)" wurde heute erstellt$/
     * @throws \Exception
     */
    public function derPostWurdeHeuteErstellt($title)
    {
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title],['postDate' => 'DESC'],1);
        $this->assertEquals((new \DateTime())->format('Y.m.d'),$post[0]->getPostDate()->format('Y.m.d'));
    }

    /**
     * @When /^der Post "([^"]*)" wurde publiziert$/
     */
    public function derPostWurdePubliziert($title)
    {
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title,'postStatus' => 'publish'],['postDate' => 'DESC'],1);
        $this->assertTrue(count($post) === 1);
    }

    /**
     * @When /^der Post "([^"]*)" wurde von dem Benutzer "([^"]*)" erstellt$/
     */
    public function derPostWurdeVonDemBenutzerErstellt($title, $userName)
    {
        $user = $this->database->getWpUserRepository()->findOneBy(['userLogin' => $userName]);
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title,'postAuthor' => $user->getId()],['postDate' => 'DESC'],1);
        $this->assertTrue(count($post) === 1);
    }

    /**
     * @When /^der Post "([^"]*)" hat den Inhalt "([^"]*)"$/
     */
    public function derPostHatDenInhalt($title, $content)
    {
        $post = $this->database->getWpPostRepository()->findBy(['postTitle' => $title],['postDate' => 'DESC'],1);
        $this->assertEquals($content,$post[0]->getPostContent());
    }

    /**
     * @When /^es gibt mindestens zwei publizierte Posts$/
     */
    public function esGibtMindestensEinenWeiterenPubliziertenPostNeben()
    {
        $posts = $this->database->getWpPostRepository()->findBy(['postStatus' => 'publish'], null,2);
        $this->assertTrue(count($posts) > 1);
    }
}